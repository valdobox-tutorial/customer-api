
const ENV = process.env

module.exports = {
  server: null,
  basePATH: ENV.basePATH || '/customer-api',
  db: {
    connectionString: ENV.MONGODB_CONNECTION_STRING,
    options: {
      useNewUrlParser: true, useFindAndModify: false
    }
  }
}
